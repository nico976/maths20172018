Problèmes de partages pour les 6e en 2017-2018
##############################################

:date: 2017-12-09
:modified: 2018-01-29
:authors: Bertrand Benjamin
:category: 6e
:tags: Opérations, Partages
:summary: 


Étape 1
=======

`3 problèmes de partage <./E1_partages.pdf>`_ pour mettre en évidence le lien entre division et partage.

 - Partage d'une quantité simple (sans reste)
 - Partage avec reste
 - Partage avec possibilité de virgule (euro)

On évite les fractions qui ne s'arrête jamais!

Bilan: on fait le lien entre partage et division.

Étape 2
=======

Problème de partage inversé  <./E2_partages.pdf>`_: on est 3 enfants, on partage des mabs, on en a  autant chacun. Combien on en a en tout?

Bilan: définition d'un multiple

Étape 2 (bis)
=============

Retrouver les multiples de 2, 3, 5 et 10

Bilan: méthode pour trouver les multiples de 2, 3, 5, 10

Étape 3
=======

`Problème de partage de butin de pirate <./E3_butin_pirate.pdf>`_

Bilan: introduction des fractions pour décrire un partage

Étape 4
=======

Banque de partage avec et sans fractions. <./E3_partages.pdf>`_



