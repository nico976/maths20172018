Problèmes de partages pour les 6e en 2017-2018
##############################################

:date: 2017-12-09
:modified: 2017-12-09
:authors: Bertrand Benjamin
:category: 6e
:tags: Opérations, Partages
:summary: 


Étape 1
=======

Banque de problèmes partages simples.

Bilan: on fait le lien entre partage et division.

Étape 2
=======

Problème de partage inversé: on est 3 enfants, on partage des mabs, on en a  autant chacun. Combien on en a en tout?

Bilan: méthode pour trouver les multiples de 2, 3, 5, 10

Étape 3
=======

Fractions de quantités simples


