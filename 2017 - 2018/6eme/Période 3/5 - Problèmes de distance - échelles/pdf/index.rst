Notion de distance et d'echelles avec les 6e pour l'année 2017-2018
###################################################################

:date: 2017-12-09
:modified: 2017-12-09
:authors: Bertrand Benjamin
:category: 6e
:tags: Géométrie, Symétrie
:summary: Manipulation du compas pour appréhender les distances et glissement vers les échelles avec les 6e pour l'année 2017-2018.


Étape 1
=======

Nuage de points questions éclaires pour compter des points

Bilan: Utilisation du compas pour améliorer la vitesse.

Étape 2
=======
Tache complexe 

Deux bateaux cherchent une épave. Ils savent qu'ils sont à la même distance. Le capitaine dit que ce n'est pas assez précis.
Et toi arriveras tu à trouver l'épave?

Bilan: définition de la médiatrice
 
Étape 3
=======

Chasse au trésors sur une carte

Bilan: utilisation d'une échelle.
 
Étape 4 
=======

Lecture de distance sur une carte
