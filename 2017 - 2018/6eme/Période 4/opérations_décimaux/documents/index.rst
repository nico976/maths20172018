

Les étapes 2 et 3 sont menées en parallèles.

Étape 1
=======

Classer des nombres décimaux  (communes autour de Kawéni). 
Les élèves cherchent et classent. Les interrogations sur l'ordre entre dixièmes et centièmes sont inscrits en bilan.

Pour résoudre ces interrogations, on donne un axe gradué pour y placer les valeurs et les classer définitivement.

Bilan: méthode pour comparer les nombres décimaux.

On continue les comparaisons en questions flash.




Étape 2
=======

E2: Equipment sportif    (Benjamin doit le mettre en forme)

E2 bis: diagramme boulangerie


Étape 3
=======

Technique de multiplication des décimaux à la main.


