Mesurer le temps avec les 6e pour l'année 2017-2018
###################################################

:date: 2018-02-24
:modified: 2018-03-10
:authors: Bertrand Benjamin
:category: 6e
:tags: Unité, Temps
:summary: Lire l'heure et convertir des unités de temps avec les 6e pour l'année 2017-2018.


Étape 1
=======

On demande aux élèves par groupe de nous dire tout ce qu'ils savent sur le temps:
- Unités
- Outils de mesure
- Temps/durée connues
- Autre chose sur le temps

Bilan avec tout ce qui est dit.

Étape 2
=======

`Lecture de l'heure sur une horloge à aiguilles <./E2_horloges.pdf>`_

On pourra ajouter des questions du genre qu'elle heure sera-t-il 5min plus tard.

Bilan: Lecture de l'heure et utilisation des 2 aiguilles.

Étape 3
=======

Positionnement des aiguilles pour afficher l'heure.

`Fiche d'horloges dans aiguilles <./E3_horloges_vides.pdf>`_ à simplifier avec seulement 2 horloges.

On pourra ajouter des questions du genre qu'elle heure sera-t-il 5min plus tard.

Étape 4
=======

Convertir des unités de temps. On pourra mettre des fractions de temps à illustrer sur des horloges.

/!\ à finir

