Approfondissement de la proportionnalité avec les 6e pour l'année 2017-2018
###########################################################################

:date: 2018-02-24
:modified: 2018-02-24
:authors: Bertrand Benjamin
:category: 6e
:tags: Opérations, Proportionnalite
:summary: Tableau de proportionnalité avec les 6e pour l'année 2017-2018.


Étape 1
=======

Tableau de proportionnalité avec la valeur de l'unité. On veut trouver les autres valeurs. On demande aux élèves d'écrire une phrase pour expliquer comment convertir.

Bilan: On a vu qu'on multipliait tout par le même nombre, on appelle ça un tableau de proportionnalité.

bilan: le tableau remplis (espace) fleche sur la droite


Étape 2
=======

Un tableau similaire mais sans la valeur de l'unité.

Bilan: Introduction du terme de coefficient de proportionnalité.


Étape 3
=======

Même chose mais le tableau est à compléter à partir d'un texte.

fiche groupe!!!!!!!!!!!!!!!!!!!!!!!!!!!!   tableau et deux phrases à ,faire
Étape 4
=======

À partir d'une carte, on cherche la taille d'objets dessinés et on veut dessiner des objets.

Calcul de la distance parcouru en une demi journée de classe.
Penser à voir avec le prof de sport pour des activités en plein air.
