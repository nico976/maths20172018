Fractions et unités divers avec les 6e pour l'année 2017-2018
#############################################################

:date: 2018-05-01
:modified: 2018-05-01
:authors: Bertrand Benjamin
:category: 6e
:tags: Unité, Fractions, 
:summary: Dernière ligne droite sur les fractions et les unités avec les 6e pour l'année 2017-2018.


Étape 1: Unités de temps
=======================

Conversion des heures vers les minutes et des minutes vers h et min.

Conversion avec les secondes aussi.

Cahier de bord: quelques conversions intéressantes avec les calculs.

Étape 2: Fraction de temps
==========================

Fraction d'heures vers minutes, addition de fractions d'heures, minutes vers fractions d'heures.

Cahier de bord: les morceaux choisis!

Étape 3: Fraction de quantité
=============================

Fractions de quantités divers avec des dessins pour le départ.

Cahier de bord: pareil

Étape 4: Fraction de surface
============================

CF p219

